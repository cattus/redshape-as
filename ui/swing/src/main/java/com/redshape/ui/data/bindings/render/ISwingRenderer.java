package com.redshape.ui.data.bindings.render;

import com.redshape.ui.data.bindings.render.components.ObjectUI;

import java.awt.*;

public interface ISwingRenderer extends IViewRenderer<Container, ObjectUI> {

}
