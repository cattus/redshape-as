package com.redshape.ui.data.bindings.views;

import com.redshape.utils.beans.bindings.types.IBindable;

public interface IPropertyModel extends IViewModel<IBindable> {
	
}
