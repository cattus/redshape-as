package com.redshape.ui.application.notifications;

/**
 * @author nikelin
 * @date 15/04/11
 * @package com.redshape.ui.notifications
 */
public enum NotificationType {
	POPUP,
	MESSAGE
}
