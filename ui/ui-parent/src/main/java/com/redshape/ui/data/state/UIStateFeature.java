package com.redshape.ui.data.state;

/**
 * @author root
 * @date 07/04/11
 * @package com.redshape.ui.state
 */
public enum UIStateFeature {
    StoragesBackup,
    SettingsBackup
}
