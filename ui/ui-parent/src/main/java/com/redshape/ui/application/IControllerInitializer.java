package com.redshape.ui.application;

public interface IControllerInitializer {
	
	public void init( IController controller );

}
