package com.redshape.jobs;

public enum JobStatus {
	FAILED,
	COMPLETED,
	WAITING,
    PROCESSING,
	PERIODICAL
}
