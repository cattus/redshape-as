package com.redshape.utils;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: May 10, 2010
 * Time: 10:51:29 PM
 * To change this template use File | Settings | File Templates.
 */
public enum CompareResult {
    LESS,
    GREATER,
    EQUALS,
    NULL
}
