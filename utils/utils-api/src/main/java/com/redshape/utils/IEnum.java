package com.redshape.utils;

import java.io.Serializable;

public interface IEnum<T> extends Serializable {
	
	public String name();
	
}
