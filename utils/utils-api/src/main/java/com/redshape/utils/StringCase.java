package com.redshape.utils;

public enum StringCase {
    UPPER,
    LOWER
}
