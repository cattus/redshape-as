package com.redshape.utils.clonners;

public interface IObjectsClonner {

	public <T> T clone( T orig );
	
}
