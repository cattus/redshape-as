package com.redshape.utils.system.scripts;

public enum ScriptListStyle {
	EXCLUSIVE,
	INCLUSIVE
}