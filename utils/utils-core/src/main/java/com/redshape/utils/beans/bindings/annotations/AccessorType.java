package com.redshape.utils.beans.bindings.annotations;

public enum AccessorType {
	METHOD,
	CONSTRUCTOR,
	FIELD
}
