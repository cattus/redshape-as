package com.redshape.utils.beans.bindings.accessors;

public interface IPropertyAccessor {

	public boolean isConsistent( Class<?> type );
	
}
