package com.redshape.utils.beans.bindings.annotations;

/**
 * @author nikelin
 * @date 25/04/11
 * @package com.redshape.bindings.annotations
 */
public enum BindableAttributes {
    LONGTEXT,
    PASSWORD,
    MULTICHOICE
}
