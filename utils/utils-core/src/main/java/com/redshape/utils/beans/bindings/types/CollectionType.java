package com.redshape.utils.beans.bindings.types;

public enum CollectionType {
	LIST,
	CHOICE
}
