package com.redshape.utils.beans.bindings.types;

public enum BindableType {
	MAP,
	LIST,
	BOOLEAN,
	OBJECT,
	NUMERIC,
	STRING,
	COMPOSITE,
	DATE,
	ENUM,
	NONE
}
