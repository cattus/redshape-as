package com.redshape.persistence.jms.core;

import com.redshape.persistence.dao.IDAO;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 * @package com.redshape.persistence.jms
 * @date 1/25/12 {4:30 PM}
 */
public interface IDao extends IDAO<EntityRecord> {

}
