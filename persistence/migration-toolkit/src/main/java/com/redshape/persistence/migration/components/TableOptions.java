package com.redshape.persistence.migration.components;

/**
 * WebCam Project
 *
 * @author nikelin
 * @project vio
 * @package com.vio.migration.components
 * @date Apr 6, 2010
 */
public enum TableOptions {
  ENGINE,
  TYPE,
  AUTO_INCREMENT,
  AVG_ROW_LENGTH,
  DEFAULT_CHARACTER_SET,
  CHARACTER_SET,
  CHECKSUM,
  DEFAULT_COLLATE,
  COLLATE,
  COMMENT,
  DELAY_KEY_WRITE,
  INDEX,
  DATA_DIRECTORY,
  INDEX_DIRECTORY,
  INSERT_METHOD,
  MAX_ROWS,
  MIN_ROWS,
  PACK_KEYS,
  PASSWORD,
  RAID_TYPE,
  RAID_CHUNKS,
  RAID_CHUNKSIZE,
  ROW_FORMAT,
  UNION,

}
