package com.redshape.persistence.migration.components;

/**
 * WebCam Project
 *
 * @author nikelin
 * @project vio
 * @package com.vio.migration.components
 * @date Apr 6, 2010
 */
public enum ReferenceOption {
    RESTRICT,
    CASCADE,
    NO_ACTION,
    SET_NULL
}
