package com.redshape.persistence.migration;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jun 29, 2010
 * Time: 10:55:00 AM
 * To change this template use File | Settings | File Templates.
 */
public enum Action {
    UPDATE,
    ROLLBACK
}
