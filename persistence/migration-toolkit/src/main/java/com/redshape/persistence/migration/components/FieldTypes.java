package com.redshape.persistence.migration.components;

/**
 * WebCam Project
 *
 * @author nikelin
 * @project vio
 * @package com.vio.migration.components
 * @date Apr 6, 2010
 */
public enum FieldTypes {
  TINYINT,
  SMALLINT,
  MEDIUMINT,
  INT,
  INTEGER,
  BIGINT,
  REAL,
  DOUBLE,
  FLOAT,
  DECIMAL,
  NUMERIC,
  DATE,
  TIME,
  TIMESTAMP,
  DATETIME,
  YEAR,
  CHAR,
  VARCHAR,
  BINARY,
  VARBINARY,
  TINYBLOB,
  BLOB,
  MEDIUMBLOB,
  LONGBLOB,
  TINYTEXT,
  TEXT,
  MEDIUMTEXT,
  LONGTEXT,
  ENUM,
  SET

}
