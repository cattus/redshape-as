package com.redshape.persistence.migration.components;

/**
 * WebCam Project
 *
 * @author nikelin
 * @project vio
 * @package com.vio.migration.components
 * @date Apr 6, 2010
 */
public enum FieldOptions {
    NOT_NULL,
    NULL,
    DEFAULT,
    AUTO_INCREMENT,
    UNIQUE_KEY,
    PRIMARY_KEY,
    COMMENT
}
