package com.redshape.persistence.migration;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jul 1, 2010
 * Time: 3:25:03 PM
 * To change this template use File | Settings | File Templates.
 */
public enum MigrationPolicy {
    DROP_CREATE,
    CREATE,
    DROP,
    UPDATE
}
