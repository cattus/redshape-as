package com.redshape.persistence.migration;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Feb 26, 2010
 * Time: 12:29:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class MigrationException extends Exception {

    public MigrationException() {}

    public MigrationException( String message ) {
        super(message);
    }

}
