package com.redshape.persistence.dao.query;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: 1/23/12
 * Time: 1:51 PM
 * To change this template use File | Settings | File Templates.
 */
public enum OrderDirection {
    ASC,
    DESC
}
