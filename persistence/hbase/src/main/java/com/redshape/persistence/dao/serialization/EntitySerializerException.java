package com.redshape.persistence.dao.serialization;

public class EntitySerializerException extends Exception {
    public EntitySerializerException() {
        super();
    }

    public EntitySerializerException(String message) {
        super(message);
    }

}
