package com.redshape.persistence.dao;

public class IndexBuilderException extends Exception {

    public IndexBuilderException() {
        super();
    }

    public IndexBuilderException(String message) {
        super(message);
    }

}

