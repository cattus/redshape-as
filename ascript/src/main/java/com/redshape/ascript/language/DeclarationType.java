package com.redshape.ascript.language;

public enum DeclarationType {
	SET,
	LIST,
	MAP,
	NEW,
	IF,
    WHEN,
	LAMBDA,
	DEFINE,
	IMPORT,
	EVAL,
	INCLUDE
}
