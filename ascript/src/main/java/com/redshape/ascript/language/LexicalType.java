package com.redshape.ascript.language;

public enum LexicalType {
	FUNCTION,
	DECLARATION,
	VARIABLE,
	LITERAL,
	STRING,
	DECLSCOPE,
	ENDSCOPE,
	GSCOPE,
	FSCOPE,
	NONE
}
