package com.redshape.ascript.language;

public interface IToken {

	public TokenType getType();
	
	public Object getValue();
	
}
