package com.redshape.ascript.language;

public enum TokenType {
	T_CONTEXT_START,
	T_CONTEXT_END,
	T_SHARP,
	T_COLON,
	T_LITERAL,
	T_STRING,
	T_NUMBER,
	T_UNKNOWN,
	T_GENERIC,
	T_SEPARATOR,
	T_PATH,
	T_END,
	T_NONE
}
