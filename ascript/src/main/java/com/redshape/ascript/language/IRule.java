package com.redshape.ascript.language;

public interface IRule<T> {

	public T getFrom();
	
	public T getTo();
	
}
