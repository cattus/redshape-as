package com.redshape.ascript.language;

public interface ITokenizer {

	public IToken process(char input);
	
}
