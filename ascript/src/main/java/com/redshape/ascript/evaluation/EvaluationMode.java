package com.redshape.ascript.evaluation;

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 * @package com.redshape.ascript.evaluation
 * @date 8/13/11 9:46 PM
 */
public enum EvaluationMode {
	EMBED,
	NORMAL
}
