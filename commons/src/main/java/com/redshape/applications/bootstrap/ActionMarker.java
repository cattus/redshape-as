package com.redshape.applications.bootstrap;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: 12.06.11
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
public enum ActionMarker {
	CRITICAL,
	PROCESSED,
	ERROR
}
