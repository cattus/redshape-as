package com.redshape.i18n;

public interface I18NManager {
	
	public String _(String id);
	
}
