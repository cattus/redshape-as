package com.redshape.net.connection.capabilities;

/**
 * @author nikelin
 * @date 13:03
 */
public interface IServerCapabilitySupport {

    public boolean isAvailable();

}
