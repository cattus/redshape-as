#very fast clean ;-)
#find . -name target -print0 |xargs rm -rf 
#rm -rf ~/.m2/repository/com/redshape/ 
#use this if you want download all jars before make
#mvn -Dmaven.test.skip=true -fn dependency:go-offline -PnetAll,renderers-all,persistence-all,uiAll,jobsAll
export MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=512m" &&
mvn -Dmaven.test.skip=true -ff -e clean install -PnetAll,renderers-all,persistence-all,uiAll,jobsAll
