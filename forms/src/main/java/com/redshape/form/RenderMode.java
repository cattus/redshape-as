package com.redshape.form;

public enum RenderMode {
	FULL,
	FIELDS,
	SUBFORMS,
	WITHOUT_DECORATORS
}
