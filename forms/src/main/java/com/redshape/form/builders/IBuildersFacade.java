package com.redshape.form.builders;

public interface IBuildersFacade {

	public IFormBuilder formBuilder();
	
	public IFormFieldBuilder formFieldBuilder();
	
}
