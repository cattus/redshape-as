package com.redshape.form.decorators;

public enum Placement {
	BEFORE,
	AFTER,
	WRAPPED
}