package com.redshape.search.serializers;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jun 30, 2010
 * Time: 6:46:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class SerializerException extends Exception {

    public SerializerException() {
        super();
    }

    public SerializerException( String message ) {
        super(message);
    }

}
