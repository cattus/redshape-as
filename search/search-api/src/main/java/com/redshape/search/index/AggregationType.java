package com.redshape.search.index;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Aug 13, 2010
 * Time: 2:32:01 PM
 * To change this template use File | Settings | File Templates.
 */
public enum AggregationType {
    ID,
    COMPOSED
}
