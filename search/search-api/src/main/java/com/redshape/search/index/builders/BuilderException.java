package com.redshape.search.index.builders;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Aug 13, 2010
 * Time: 4:06:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class BuilderException extends Exception {

    public BuilderException() {
        super();
    }

    public BuilderException( String message ) {
        super(message);
    }

	public BuilderException( String message, Throwable e ) {
		super(message, e );
	}

}
