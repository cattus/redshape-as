package com.redshape.search.index.visitor;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Aug 13, 2010
 * Time: 3:25:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class VisitorException extends Exception {

    public VisitorException() {
        super();
    }

    public VisitorException( String message ) {
        super(message);
    }

	public VisitorException( String message, Throwable e ) {
		super(message, e);
	}

}
