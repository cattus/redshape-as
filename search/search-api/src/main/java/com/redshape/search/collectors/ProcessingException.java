package com.redshape.search.collectors;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jun 30, 2010
 * Time: 4:14:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProcessingException extends Exception {

    public ProcessingException() {
        super();
    }

    public ProcessingException( String message ) {
        super(message);
    }

	public ProcessingException( String message, Throwable e ) {
		super(message, e);
	}
    
}
