package com.redshape.search.index;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jun 29, 2010
 * Time: 2:52:48 PM
 * To change this template use File | Settings | File Templates.
 */
public enum IndexingType {
    TEXT,
    BINARY,
    NUMERICAL,
    DATE,
    CUSTOM
}
