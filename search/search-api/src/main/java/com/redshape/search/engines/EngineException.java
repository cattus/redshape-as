package com.redshape.search.engines;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Jun 29, 2010
 * Time: 4:24:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class EngineException extends Exception {

    public EngineException() {
        super();
    }

    public EngineException( String message ) {
        super(message);
    }

	public EngineException( String message, Throwable e ) {
		super(message, e);
	}
}
