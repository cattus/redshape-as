package com.redshape.servlet.views;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: 10/11/10
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class View extends AbstractView {

    public View( String basePath, String extension ) {
        this( basePath, null, extension );
    }

    public View( String basePath, String scriptPath, String extension ) {
        super( basePath, scriptPath, extension );
    }

}
