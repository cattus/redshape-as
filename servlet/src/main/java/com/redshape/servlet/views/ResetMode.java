package com.redshape.servlet.views;

/**
 * @package com.redshape.servlet.views
 * @user cyril
 * @date 6/21/11 1:04 AM
 */
public enum ResetMode {
    FULL,
    TRANSIENT,
    VOLATILE
}
