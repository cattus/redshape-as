package com.redshape.servlet.core;

/**
 * @author nikelin
 * @date 13:56
 */
public enum SupportType implements Comparable<SupportType> {
    NO,
    MAY,
    SHOULD,
    MUST;
}
