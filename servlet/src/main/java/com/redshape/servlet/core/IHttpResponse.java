package com.redshape.servlet.core;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: 10/11/10
 * Time: 12:09 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IHttpResponse extends HttpServletResponse {
}
