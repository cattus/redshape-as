package com.redshape.io.net.adapters.socket.events;

/**
 * Created by IntelliJ IDEA.
 * User: nikelin
 * Date: Feb 8, 2010
 * Time: 12:34:56 AM
 * To change this template use File | Settings | File Templates.
 */
public enum SocketAdapterEvent {
    ACCEPTED,
    CLOSED,
    BINDED,
    INIT
}
