package com.redshape.io.net;

import java.net.InetAddress;

/**
 * WebCam Project
 *
 * @author nikelin
 * @project vio
 * @package com.vio.persistence.entities
 * @date Apr 17, 2010
 */
public interface IAddress {

	public InetAddress getInetAddress();
	
}
