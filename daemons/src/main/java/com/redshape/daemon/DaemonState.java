package com.redshape.daemon;

public enum DaemonState {
	 INITIALIZED,
	 STARTED,
	 STOPPED,
	 ERROR
}
