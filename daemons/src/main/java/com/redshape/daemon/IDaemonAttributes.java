package com.redshape.daemon;

import com.redshape.utils.IEnum;

public interface IDaemonAttributes extends IEnum<IDaemonAttributes> {
	
	public String name();
	
}